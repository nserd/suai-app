package com.keaoc.nserd.suai_app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import static com.keaoc.nserd.suai_app.SuaiAppConstants.COOKIES_MODE;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.PIN_HASH;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.SP_PIN;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.SP_SETTINGS;

public class EntryActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_entry);

        new Handler().postDelayed(new Runnable() {
            Intent intent;

            @Override
            public void run() {
                if(isPinExist() && !isCookieModeOn()){
                    intent = new Intent(EntryActivity.this, AccountActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.anim_entry, R.anim.anim_entry2);
                }
                else{
                    intent = new Intent(EntryActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.anim_entry, R.anim.anim_entry2);
                }
            }
        }, 2000);
    }

    @Override
    protected void onStart(){
        super.onStart();

        ImageView logo = findViewById(R.id.entryLogo);
        TextView text = findViewById(R.id.entryName);

        Typeface font;
        Animation anim;

        font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        text.setTypeface(font);

        anim = AnimationUtils.loadAnimation(this,R.anim.anim_scale);
        logo.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this,R.anim.anim_entry_name);
        text.startAnimation(anim);
    }

    private boolean isPinExist(){
        SharedPreferences sPref;
        sPref = getSharedPreferences(SP_PIN, MODE_PRIVATE);

        return sPref.getString(PIN_HASH, null) != null;
    }

    //временный фикс
    private boolean isCookieModeOn(){
        SharedPreferences settingsSP = getSharedPreferences(SP_SETTINGS, MODE_PRIVATE);
        return settingsSP.getBoolean(COOKIES_MODE, false);
    }
}