package com.keaoc.nserd.suai_app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.keaoc.nserd.suai_app.network.Cookie;

import static com.keaoc.nserd.suai_app.AuxiliaryFunc.eraseAllData;
import static com.keaoc.nserd.suai_app.AuxiliaryFunc.getCustomID;
import static com.keaoc.nserd.suai_app.AuxiliaryFunc.sha256;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.PIN_ATTEMPTS_COUNTER;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.PIN_HASH;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.SP_PIN;

public class PinCodeActivity extends AppCompatActivity {
    private final String TAG = "PIN_Activity";

    //TODO исправить баг с вылетом приложения при быстром вводе пина

    private final int MAX_PIN_LENGTH = 5;
    private final int MAX_ATTEMPTS   = 3;
    private final int DEFAULT_LABEL  = 0;
    private final int WRONG_LABEL    = 1;

    private ImageView[] dots;
    private StringBuilder pin;

    private TextView pinLabel;

    private LabelDelayThread dlThread;
    private Handler labelHandler;

    private int dotsCounter;

    SharedPreferences sPref;

    private boolean newPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_code);

        sPref = getSharedPreferences(SP_PIN, MODE_PRIVATE);

        pin = new StringBuilder();

        //-----------init Views------------------
        pinLabel = findViewById(R.id.pin_label);
        dots = initDots();
        initNumPad();
        //---------------------------------------

        labelHandler = initLabelHandler();
        dotsCounter = 0;

        if(sPref.getString(PIN_HASH, null) == null ){
            pinLabel.setText("Придумайте ПИН-код для входа");
            pinLabel.setTextSize(22);
        }
    }

    @Override
    protected void onStop(){
        super.onStop();

        if(newPin)
            eraseAllData(this);
    }

    @Override
    protected void onStart(){
        super.onStart();
        if(newPin) {
            Intent intent = new Intent(PinCodeActivity.this, EntryActivity.class);
            startActivity(intent);

            finish();
        }
    }

    private ImageView[] initDots(){
        ImageView[] dots = new ImageView[MAX_PIN_LENGTH];

        dots[0] = findViewById(R.id.dot0);
        dots[1] = findViewById(R.id.dot1);
        dots[2] = findViewById(R.id.dot2);
        dots[3] = findViewById(R.id.dot3);
        dots[4] = findViewById(R.id.dot4);

        return dots;
    }

    private void initNumPad(){
        Button[] numbButtons = new Button[10];
        Button backButton, exitButton;

        numbButtons[0] = findViewById(R.id.numb_button_0);
        numbButtons[1] = findViewById(R.id.numb_button_1);
        numbButtons[2] = findViewById(R.id.numb_button_2);
        numbButtons[3] = findViewById(R.id.numb_button_3);
        numbButtons[4] = findViewById(R.id.numb_button_4);
        numbButtons[5] = findViewById(R.id.numb_button_5);
        numbButtons[6] = findViewById(R.id.numb_button_6);
        numbButtons[7] = findViewById(R.id.numb_button_7);
        numbButtons[8] = findViewById(R.id.numb_button_8);
        numbButtons[9] = findViewById(R.id.numb_button_9);

        backButton = findViewById(R.id.numb_button_back);
        exitButton = findViewById(R.id.numb_button_exit);

        //-Add text
        for (int i = 0; i < numbButtons.length; i++) {
            String numb = i + "";
            numbButtons[i].setText(numb);
        }

        backButton.setText("");
        exitButton.setText("Выход");

        //-Add font
        Typeface font = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");
        for (Button numbButton : numbButtons) numbButton.setTypeface(font);

        font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        exitButton.setTypeface(font);
        exitButton.setTextSize(15);
    }
    @NonNull
    private Handler initLabelHandler(){
        return new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                if(msg.what == DEFAULT_LABEL)
                    setDefaultLabel();

                if(msg.what == WRONG_LABEL){
                    pinLabel.setText("Неверный ПИН-код. Попробуйте еще раз");
                    pinLabel.setTextColor(getResources().getColor(R.color.sRed1));
                    pinLabel.setTextSize(22);
                }
            }
        };
    }

    private void setDefaultLabel(){
        pinLabel.setText("Введите ПИН-код");
        pinLabel.setTextColor(getResources().getColor(R.color.suaiDarkBlue));
        pinLabel.setTextSize(30);
    }

    private void setWrongLabel(){
        dlThread = new LabelDelayThread(labelHandler, 1500);
        dlThread.start();
    }

    public void onClickNumPad(View view) {
        Animation scale_dots = AnimationUtils.loadAnimation(this, R.anim.anim_dots);

        setVibration((Button)view);

        switch (view.getId()){
            case R.id.numb_button_back :
                if(pin.length() > 0) deleteNumb(scale_dots);

                break;

            case R.id.numb_button_exit :
                AuxiliaryFunc.eraseAllData(this);

                Log.i(TAG, "Old PIN deleted!");

                Intent loginActivity = new Intent(PinCodeActivity.this, LoginActivity.class);
                startActivity(loginActivity);

                finish();

                break;

            default:
                if(pinLabel.getText().equals("Неверный ПИН-код. Попробуйте еще раз")) {
                    dlThread.kill();
                    setDefaultLabel();
                }

                addNumb(view, scale_dots);

                if (pin.length() == MAX_PIN_LENGTH) checkPIN();

                break;
        }
    }

    private void addNumb(View view, Animation scale_dots){
        dots[dotsCounter].setImageDrawable(getResources().getDrawable(R.drawable.dot_filled));
        dots[dotsCounter].startAnimation(scale_dots);

        pin.append(((Button) view).getText());

        dotsCounter++;
    }

    private void deleteNumb(Animation anim){
        pin.delete(pin.length() - 1, pin.length());
        dotsCounter--;

        dots[dotsCounter].setImageDrawable(getResources().
                getDrawable(R.drawable.dot_empty));

        dots[dotsCounter].startAnimation(anim);
    }

    private void deleteAllNumb(boolean useAnim){
        pin = new StringBuilder();
        dotsCounter = 0;

        if(useAnim) {
            Animation shake_dots = AnimationUtils.loadAnimation(this, R.anim.shake);

            for (ImageView dot : dots) {
                dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_empty));
                dot.startAnimation(shake_dots);
            }
        }

        else {
            for (ImageView dot : dots)
                dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_empty));
        }
    }

    //TODO: Перепроверить алгоритм, уменьшить количество if else
    private void checkPIN(){
        String storedHash = sPref.getString(PIN_HASH, null);

        if (storedHash == null) {
            String hash = sha256(pin.toString() + getCustomID(this));
            savePIN(hash);

            deleteAllNumb(false);
            newPin = true;

            pinLabel.setText("Введите ПИН-код еще раз");
            pinLabel.setTextSize(22);

            Log.i(TAG, "PIN not found");
            Log.i(TAG, "Create new PIN [step 1]");
        }

        else {
            String hash = sha256(pin.toString() + getCustomID(this));

            if (hash.equals(storedHash)) {
                Intent intent = getIntent();

                boolean initialPin = intent.getBooleanExtra("initial_pin", true);
                intent.putExtra("dbkey", AuxiliaryFunc.aesCipherENC(this, getDbKey()));

                if(initialPin)
                    setResult(RESULT_FIRST_USER, intent);
                else
                    setResult(RESULT_OK);

                if(newPin){
                    Log.i(TAG, "Create new PIN [step 2]");
                    Log.i(TAG, "PIN successfully created!");

                    newPin = false;
                }
                else
                    Log.i(TAG, "PIN Correct!");

                resetAttemptsCounter();
                finish();
                overridePendingTransition(R.anim.bottom_up, R.anim.top_out);
            }
            else {
                if(newPin){
                    deleteAllNumb(true);
                    deletePIN();

                    pinLabel.setText("ПИН-коды не совпадают. Попробуйте снова");
                    newPin = false;
                }
                else {
                    setWrongLabel();
                    deleteAllNumb(true);

                    int aCounter;

                    aCounter = sPref.getInt(PIN_ATTEMPTS_COUNTER, 0);
                    aCounter++;

                    checkAttempts(aCounter);
                }
            }
        }
    }

    private void checkAttempts(int attemptsCounter){
        if(attemptsCounter > MAX_ATTEMPTS){
            AuxiliaryFunc.eraseAllData(this);
            Log.i(TAG, "Too many attempts. Account Data deleted.");

            Intent loginActivity = new Intent(PinCodeActivity.this, LoginActivity.class);
            startActivity(loginActivity);

            Toast.makeText(getApplicationContext(),
                    "Превышено количество попыток ввода ПИН-кода", Toast.LENGTH_SHORT).show();

            finish();
        }
        else {
            SharedPreferences.Editor editor = sPref.edit();
            editor.putInt(PIN_ATTEMPTS_COUNTER, attemptsCounter);
            editor.apply();

            Log.i(TAG, "Wrong PIN! Left attempts: " + (MAX_ATTEMPTS - attemptsCounter));
        }
    }

    private void savePIN(String hash){
        SharedPreferences.Editor editor = sPref.edit();

        editor.putString(PIN_HASH, hash);
        editor.apply();
    }

    private void setVibration(Button b){
        b.setHapticFeedbackEnabled(true);
        b.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
    }

    private void deletePIN(){
        SharedPreferences.Editor editor = sPref.edit();

        editor.clear();
        editor.apply();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED);
    }

    private void resetAttemptsCounter(){
        SharedPreferences.Editor editor = sPref.edit();
        editor.putInt(PIN_ATTEMPTS_COUNTER, 0);
        editor.apply();
    }

    private String getDbKey(){
        String sessID = Cookie.getCookieValue("PHPSESSID", this);

        return sha256(pin.toString() + sessID);
    }

    class LabelDelayThread extends Thread{
        private Handler handler;
        private int delay;

        private boolean stopThread = false;

        LabelDelayThread(Handler handler, int delay){
            this.handler = handler;
            this.delay   = delay;
        }

        @Override
        public void run() {
            super.run();

            handler.sendEmptyMessage(WRONG_LABEL);

            long currentTime = System.currentTimeMillis();

            while (!isTime(currentTime, delay))
                if (stopThread) return;

            handler.sendEmptyMessage(DEFAULT_LABEL);
        }

        void kill(){
            stopThread = true;
            Log.i(TAG, "Delay thread " + getName() + " kill");
        }

        private boolean isTime(long startTime, int delay){
            return System.currentTimeMillis() - startTime >= delay;
        }
    }
}