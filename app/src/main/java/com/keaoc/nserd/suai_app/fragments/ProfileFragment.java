package com.keaoc.nserd.suai_app.fragments;

import android.content.ContextWrapper;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.keaoc.nserd.suai_app.AccountActivity;
import com.keaoc.nserd.suai_app.R;
import com.keaoc.nserd.suai_app.SuaiAppConstants;
import com.keaoc.nserd.suai_app.fragments.tabs.AchievementsFragment;
import com.keaoc.nserd.suai_app.fragments.tabs.EducationsFragment;
import com.keaoc.nserd.suai_app.fragments.tabs.PublicationsFragment;
import com.keaoc.nserd.suai_app.fragments.tabs.SubjectsFragment;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment {
    private final String TAG = "ProfileFragment";

    TextView firstName;
    TextView secondName;
    TextView middleName;
    TextView positions;

    CircleImageView profileImg;

    LinearLayout cItemContainer;
    LinearLayout pItemContainer;

    //кидать сюда json??
    public static ProfileFragment newInstance(String key) {
        Bundle args = new Bundle();

        args.putString("key", key);

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //TODO: Баг с курсором (краш после закрытия приложения)
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        firstName  = view.findViewById(R.id.profileFirName);
        secondName = view.findViewById(R.id.profileSecName);
        middleName = view.findViewById(R.id.profileMidName);
        positions  = view.findViewById(R.id.positionsProfile);

        profileImg = view.findViewById(R.id.profileImageFrag);

        cItemContainer = view.findViewById(R.id.cItemContainer);
        pItemContainer = view.findViewById(R.id.pItemContainer);

        LinearLayout infoButton = view.findViewById(R.id.infoButton);
        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick in");

                if(getActivity() != null)
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            createTabsWithFragments(getActivity());
                        }
                    }, 200);
                else
                    Log.e(TAG, "getActivity is null");
            }
        });

        AccountActivity accountActivity = (AccountActivity) getActivity();
        Handler handler = initHandler(accountActivity);

        new LoadData(handler).start();

        return view;
    }

    protected Uri getImageFromInternalStorage(String name){
        ContextWrapper wrapper = new ContextWrapper(getContext());

        File file = wrapper.getDir("Images",MODE_PRIVATE);
        file = new File(file, name +".jpg");

        return Uri.parse(file.getAbsolutePath());
    }

    @NonNull
    private Handler initHandler(final AccountActivity accountActivity){
        return new Handler(Looper.getMainLooper()){

            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                SQLiteDatabase db = accountActivity.getDatabase();
                getData(db);

                profileImg.setImageURI(getImageFromInternalStorage("ProfileImg"));
            }
        };
    }

    class LoadData extends Thread {
        Handler handler;

        LoadData(Handler handler){
            this.handler = handler;
        }

        @Override
        public void run() {
            handler.sendEmptyMessage(0);
        }
    }

    private void createTabsWithFragments(FragmentActivity activity){
        TabLayout tabs      = activity.findViewById(R.id.tabs);
        ViewPager viewPager = activity.findViewById(R.id.viewPager);

        FragmentManager     fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction().addToBackStack("stack");

        Fragment fragment = fm.findFragmentByTag(SuaiAppConstants.PROFILE_FRAG);

        ft.detach(fragment);
        ft.commit();

        tabs.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.VISIBLE);

        SuaiFragmentPagerAdapter fa = new SuaiFragmentPagerAdapter(fm);

        fa.addFrag(new AchievementsFragment(), "Достижения");
        fa.addFrag(new EducationsFragment(),   "Образование");
        fa.addFrag(new PublicationsFragment(),  "Публикации");
        fa.addFrag(new SubjectsFragment(),     "Дисциплины");

        viewPager.setAdapter(fa);

        tabs.setupWithViewPager(viewPager);

        AccountActivity.fragmentChangeFlag = SuaiAppConstants.TABS_FRAGS;
        Log.d(TAG, "Tabs created.");
    }

    private class SuaiFragmentPagerAdapter extends FragmentPagerAdapter {
        private  final List<Fragment> mFragmentList = new ArrayList<>();
        private  final List<String> mTitleList = new ArrayList<>();

        SuaiFragmentPagerAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);
        }

        void addFrag(Fragment fragment, String title){
            mFragmentList.add(fragment);
            mTitleList.add(title);
        }
    }

    private void createContactsCard(String a, String e, String p){
        if(a.length() > 0)
            cItemContainer.addView(createContactsCardItem
                    ("Аудитория", a, R.drawable.ic_home_black_100dp));
        if(e.length() > 0)
            cItemContainer.addView(createContactsCardItem
                    ("Email", e, R.drawable.ic_email_black_100dp));
        if(p.length() > 0)
            cItemContainer.addView(createContactsCardItem
                    ("Телефоны", p, R.drawable.ic_phone_black_100dp));
    }

    private View createContactsCardItem(String labelString, String textString, int drawRes){
        LayoutInflater factory = LayoutInflater.from(getContext());
        View cardItem = factory.inflate(R.layout.item_contacts, null);

        ImageView icon  = cardItem.findViewById(R.id.item_cont_icon);
        TextView  label = cardItem.findViewById(R.id.item_cont_label);
        TextView  text  = cardItem.findViewById(R.id.item_cont_text);

        icon.setImageDrawable(getActivity().getResources().getDrawable(drawRes));
        label.setText(labelString);
        text.setText(textString);

        return cardItem;
    }

    private View createPositionsCardItem(String postString, String facultyString,
                                         String depnameShortString){

        LayoutInflater factory = LayoutInflater.from(getContext());
        View cardItem = factory.inflate(R.layout.item_positions, null);

        TextView post = cardItem.findViewById(R.id.post);
        TextView faculty = cardItem.findViewById(R.id.faculty);
        TextView depnameShort = cardItem.findViewById(R.id.depname);

        post.setText(postString);
        faculty.setText(facultyString);
        depnameShort.setText(depnameShortString);

        return cardItem;
    }

    private void getData(SQLiteDatabase db){
        getDataUser(db);
    }

    private void getDataUser(SQLiteDatabase db){
        String contacts_auditorium = "";
        String contacts_email      = "";
        String contacts_phone      = "";

        Cursor c = db.query(SuaiAppConstants.USER, new String[] {"key", "value"},
                null, null, null, null, null);

        c.moveToFirst();

        int keyIndex = c.getColumnIndex("key");
        int valueIndex = c.getColumnIndex("value");

        StringBuilder posSb = new StringBuilder();

        do{
            String key = c.getString(keyIndex);

            switch (key){
                case "firstname":   firstName.setText(c.getString(valueIndex));  break;
                case "lastname":    secondName.setText(c.getString(valueIndex)); break;
                case "middlename":  middleName.setText(c.getString(valueIndex)); break;
                case "degree_name": posSb.append(c.getString(valueIndex));       break;
                case "at_name":     posSb.append(c.getString(valueIndex));       break;

                case "auditorium":
                    contacts_auditorium = c.getString(valueIndex);
                    break;

                case "email":
                    contacts_email = c.getString(valueIndex);
                    break;

                case "phone":
                    contacts_phone = c.getString(valueIndex);
                    break;
            }

        } while (c.moveToNext());
        c.close();

        positions.setText(posSb.toString());
        createContactsCard(contacts_auditorium, contacts_email, contacts_phone);

        c = db.query(SuaiAppConstants.WORKS, new String[] {"post", "faculty", "depname_short"},
                null, null, null, null, null);

        c.moveToFirst();

        int postIndex = c.getColumnIndex("post");
        int facultyIndex = c.getColumnIndex("faculty");
        int depnameShortIndex = c.getColumnIndex("depname_short");

        do{
            if(postIndex == -1 || facultyIndex == -1 || depnameShortIndex == -1)
                break;

            String post = c.getString(postIndex);
            String faculty = c.getString(facultyIndex);
            String depnameShort = c.getString(depnameShortIndex);

            pItemContainer.addView(createPositionsCardItem(post, faculty, depnameShort));

        } while (c.moveToNext());
        c.close();
    }
}