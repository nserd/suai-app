package com.keaoc.nserd.suai_app;

/**
 * Created by nserd on 23.05.18.
 */
public class SuaiAppConstants {
    public static final String HOST = "pro.guap.ru";

    //Shared Preferences repositories
    static final String SP_SAVED_LOGIN = "saved_login";
    static final String SP_PIN         = "PIN";
    public static final String SP_SETTINGS    = "settings";

    //PIN repositories
    static final String PIN_HASH             = "hash";
    static final String PIN_ATTEMPTS_COUNTER = "attempts_counter";

    //Settings repositories
    public static final String COOKIES_MODE = "cookies_mode";

    //Database constants
    public static final String WORKS = "works";
    static final String PUBLICATION  = "publication";
    public static final String ACHIEVEMENTS = "achievements";
    public static final String SUBJECTS     = "subgects";
    public static final String USER  = "user";
    public static final String EDUCATIONS_NAMES = "edu_names";
    public static final String PUBLICATIONS_NAMES = "pub_names";

    //Main fragments
    public static final String PROFILE_FRAG = "profile";
    static final String PUBLICATIONS_FRAG   = "publications";
    static final String PEOPLE_FRAG      = "people";
    static final String MATERIALS_FRAG      = "materials";
    static final String TASKS_FRAG          = "tasks";
    static final String REPORTS_FRAG        = "reports";
    static final String DISCIPLINES_FRAG    = "disciplines";
    static final String DOCUMENTS_FRAG      = "documents";
    static final String LIBRARY_FRAG        = "library";
    static final String SETTINGS_FRAG       = "settings";

    public static final String TABS_FRAGS   = "tabs";

    //Database tables
    static final String CREATE_WORKS_TABLE = "create table " + WORKS + " (" +
                                             "id integer primary key autoincrement," +
                                             "tid integer," +
                                             "post text," +
                                             "depname text," +
                                             "faculty text," +
                                             "depname_short text," +
                                             "faculty_short text," +
                                             "pluralist integer" +
                                             ");";

    static final String CREATE_PUBLICATION_TABLE = "create table " + PUBLICATION + " (" +
                                                   "id integer primary key autoincrement," +
                                                   "sid integer," +
                                                   "user_id integer," +
                                                   "title text," +
                                                   "category integer," +
                                                   "year integer," +
                                                   "pub_public integer," +
                                                   "publishing_office text," +
                                                   "pagescount integer," +
                                                   "isbn text," +
                                                   "pub_magazine_id," +
                                                   "pages text," +
                                                   "conference text," +
                                                   "authors text," +
                                                   "pub_eng_name text," +
                                                   "pub_trans_name text," +
                                                   "magazineName," +
                                                   "pub_file_name," +
                                                   "pub_file_hash" +
                                                   ");";

    static final String CREATE_ACHIEVEMENTS_TABLE = "create table " + ACHIEVEMENTS + " (" +
                                                    "id integer primary key autoincrement," +
                                                    "sid integer," +
                                                    "text text" +
                                                    ");";

    static final String CREATE_SUBJECTS_TABLE = "create table " + SUBJECTS + " (" +
                                                "id integer primary key autoincrement," +
                                                "name text" +
                                                ");";

    static final String CREATE_USER_TABLE = "create table " + USER + " (" +
                                            "id integer primary key autoincrement," +
                                            "key text," +
                                            "value text" +
                                            ");";

    static final String CREATE_EDUCATIONS_TABLE = " (" +
                                                  "id integer primary key autoincrement," +
                                                  "institute text," +
                                                  "category_id integer," +
                                                  "eduform integer," +
                                                  "qualification integer," +
                                                  "country text," +
                                                  "city text," +
                                                  "department text," +
                                                  "chair text," +
                                                  "speciality text," +
                                                  "year integer," +
                                                  "dissertation_year integer," +
                                                  "dissertation_topic text," +
                                                  "course_start text," +
                                                  "course_end text," +
                                                  "edu_name text," +
                                                  "engName text," +
                                                  "edu_form_name text," +
                                                  "edu_qualification_name text" +
                                                  ");";

    static final String CREATE_PUBLICATIONS_TABLE = " (" +
                                                    "id integer primary key autoincrement," +
                                                    "sid integer," +
                                                    "user_id integer," +
                                                    "title text," +
                                                    "category integer," +
                                                    "year integer," +
                                                    "pub_public integer," +
                                                    "publishing_office text," +
                                                    "pagescount integer," +
                                                    "isbn text," +
                                                    "pub_magazine_id integer," +
                                                    "pages text," +
                                                    "conference text," +
                                                    "authors text," +
                                                    "pub_eng_name text," +
                                                    "pub_trans_name text," +
                                                    "magazineName text," +
                                                    "pub_file_name text," +
                                                    "pub_file_hash text" +
                                                    ");";

    static final String CREATE_EDUCATIONS_NAMES_TABLE = "create table " + EDUCATIONS_NAMES + " (" +
                                                        "id integer primary key autoincrement," +
                                                        "name text" +
                                                        ");";

    static final String CREATE_PUBLICATIONS_NAMES_TABLE = "create table " + PUBLICATIONS_NAMES + " (" +
                                                          "id integer primary key autoincrement," +
                                                          "name text" +
                                                          ");";
}
