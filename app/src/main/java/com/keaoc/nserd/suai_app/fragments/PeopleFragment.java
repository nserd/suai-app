package com.keaoc.nserd.suai_app.fragments;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.support.v7.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.keaoc.nserd.suai_app.AccountActivity;
import com.keaoc.nserd.suai_app.R;
import com.keaoc.nserd.suai_app.network.DataSender;
import com.keaoc.nserd.suai_app.network.NoAutoRedirect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.keaoc.nserd.suai_app.SuaiAppConstants.HOST;

public class PeopleFragment extends Fragment {
    AccountActivity accountActivity;

    LinearLayout peopleContainer;

    Button loadMorePeople;

    String searchStr;
    int offset;


    public PeopleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_people, container, false);

        accountActivity = (AccountActivity) getActivity();
        accountActivity.searchItem.setVisible(true);

        searchStr = "";
        offset = 0;

        peopleContainer = view.findViewById(R.id.people_container);
        loadMorePeople  = view.findViewById(R.id.loadMorePeopleButton);
        loadMorePeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offset += 10;
                setRequestPeople(setJsonParams(offset, searchStr));
            }
        });

        setRequestPeople(setJsonParams(offset, "0"));
//
        SearchView searchView = (SearchView) accountActivity.searchItem.getActionView();
        searchView.setOnQueryTextListener(new MyOnQueryTextListener());

        return view;
    }

    private JSONObject setJsonParams(int offset, String searchText){
        JSONObject params = new JSONObject();

        if(searchText.equals(""))
            searchText = "0";

        try {
            params.put("chair", 0);
            params.put("limit", 10);
            params.put("offset", offset);
            params.put("people", 0);
            params.put("post", 0);
            params.put("role", 1);
            params.put("searchtext", searchText);
            params.put("subdivision", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return params;
    }

    private void setRequestPeople(JSONObject params){
        String url = "http://" + HOST + "/getDictionariesAndPeople/";

        final DataSender dataSender;
        final RequestQueue queue = accountActivity.queue;

        dataSender = new DataSender(queue, HOST, (AppCompatActivity) getActivity()){
            @Override
            public void doOnAnswer(JSONObject response) {
                super.doOnAnswer(response);

                try {
                    getPeopleData(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        accountActivity.showProgressBar();
        dataSender.sendJSONRequest(url, params, queue, Request.Method.POST);
    }

    private void getPeopleData(JSONObject jO) throws JSONException {
        RequestQueue queue = Volley.newRequestQueue(getContext(), new NoAutoRedirect());
        JSONArray people = jO.getJSONArray("people");

        if(people.length() != 0)
            loadMorePeople.setVisibility(View.VISIBLE);
        else
            loadMorePeople.setVisibility(View.INVISIBLE);

        for(int i = 0; i < people.length(); i++){
            JSONObject data = (JSONObject) people.get(i);

            StringBuilder name = new StringBuilder();
            StringBuilder info = new StringBuilder();

            name.append(data.get("lastname"));
            name.append(" ");
            name.append(data.get("firstname"));
            name.append(" ");
            name.append(data.get("middlename"));

            info.append(data.get("post"));
            info.append(", ");
            info.append(data.get("depname"));
            info.append(", ");
            info.append(data.get("faculty"));

            String imageUrl = (String) data.get("image");

            View item = createItem(queue, imageUrl, name.toString(), info.toString());
            peopleContainer.addView(item);
        }

        accountActivity.hideProgressBar();
    }

    private View createItem(RequestQueue queue, String imgURL, String nameStr, String infoStr){
        LayoutInflater factory = LayoutInflater.from(getContext());
        View peopleItem = factory.inflate(R.layout.item_people, null);

        CircleImageView profileImage = peopleItem.findViewById(R.id.peopleImage);

        TextView  name = peopleItem.findViewById(R.id.peopleName);
        TextView  info = peopleItem.findViewById(R.id.peopleInfo);

        getAccountImage(queue, imgURL, profileImage);

        name.setText(nameStr);
        info.setText(infoStr);

        return  peopleItem;
    }

    private void getAccountImage(RequestQueue queue, String url, final CircleImageView container){
        if(!url.contains(HOST))
            url = "http://" + HOST + url;

        DataSender dataSender = new DataSender(queue, HOST, accountActivity) {
            @Override
            public void doOnAnswer(Bitmap response) {
                super.doOnAnswer(response);

                container.setImageBitmap(response);
            }
        };

        dataSender.sendImageRequest(url, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.RGB_565);
    }

    class MyOnQueryTextListener implements SearchView.OnQueryTextListener {

        @Override
        public boolean onQueryTextSubmit(String query) {
            searchStr = query;

            peopleContainer.removeAllViews();
            setRequestPeople(setJsonParams(0, searchStr));
            return false;
        }

        //TODO: доделать поиск
        @Override
        public boolean onQueryTextChange(String newText) {
            if(newText.length() == 0){
                searchStr = newText;
                offset = 0;

                peopleContainer.removeAllViews();
                setRequestPeople(setJsonParams(0, searchStr));
            }

            return false;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        accountActivity.searchItem.setVisible(false);
    }
}