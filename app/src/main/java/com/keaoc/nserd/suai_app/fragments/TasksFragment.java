package com.keaoc.nserd.suai_app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.keaoc.nserd.suai_app.AccountActivity;
import com.keaoc.nserd.suai_app.R;
import com.keaoc.nserd.suai_app.network.DataSender;
import com.keaoc.nserd.suai_app.network.NoAutoRedirect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.keaoc.nserd.suai_app.SuaiAppConstants.HOST;

public class TasksFragment extends Fragment {

    AccountActivity accountActivity;
    LinearLayout tasksContainer;

    public TasksFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tasks, container, false);

        accountActivity = (AccountActivity) getActivity();
        tasksContainer  = view.findViewById(R.id.tasksContainer);

        setRequestTasks(setJsonParams(accountActivity.userId));

        return view;
    }

    private JSONObject setJsonParams(String userId){
        JSONObject params = new JSONObject();

        try {
            params.put("iduser", userId);
            params.put("limit", 100);
            params.put("offset", 0);
            params.put("semester", 0);
            params.put("subject", 0);
            params.put("type", 0);

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return params;
    }

    //TODO: приходят не все задания
    private void setRequestTasks(JSONObject params){
        String url = "http://" + HOST + "/gettasks/";

        final DataSender dataSender;
        final RequestQueue queue = accountActivity.queue;

        dataSender = new DataSender(queue, HOST, (AppCompatActivity) getActivity()){
            @Override
            public void doOnAnswer(JSONObject response) {
                super.doOnAnswer(response);

                try {
                    getTasksData(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        accountActivity.showProgressBar();
        dataSender.sendJSONRequest(url, params, queue, Request.Method.POST);
    }

    private void getTasksData(JSONObject data) throws JSONException {
        JSONArray materials = data.getJSONArray("tasks");

        for(int i = 0; i < materials.length(); i++){
            if(materials.get(i) instanceof JSONObject) {
                JSONObject item = (JSONObject) materials.get(i);

                String subjName = (String) item.getJSONArray("subject_name").get(0);
                String nameStr  = item.getString("name");

                int doneWork = item.getInt("doneWork");
                int count    = item.getInt("count");
                int await    = item.getInt("await");

                String dataStr = item.getString("datecreate");

                tasksContainer.addView(createItem(subjName, nameStr, doneWork, count, await,
                        dataStr));
            }
        }

        accountActivity.hideProgressBar();
    }

    private View createItem(String sNameStr, String nameStr, int doneWork, int count, int await,
                            String dateStr){
        CardView cardView = new CardView(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(dpToPx(8),dpToPx(8),dpToPx(8), 1);

        cardView.setLayoutParams(params);
        LayoutInflater factory = LayoutInflater.from(getContext());
        View tasksItem = factory.inflate(R.layout.item_tasks, null);

        TextView subjName  = tasksItem.findViewById(R.id.tasks_discipline);
        TextView name      = tasksItem.findViewById(R.id.tasks_name);
        TextView completed = tasksItem.findViewById(R.id.tasks_completed);
        TextView wait      = tasksItem.findViewById(R.id.tasks_wait);
        TextView date      = tasksItem.findViewById(R.id.tasks_date);

        subjName.setText(sNameStr);
        name.setText(nameStr);
        completed.setText("Выполнили " + doneWork + " из " + count + " студентов");
        wait.setText(await + "");
        date.setText(dateStr);

        cardView.addView(tasksItem);

        return cardView;
    }

    private int dpToPx(int dp){
        return (int) (dp * getContext().getResources().getDisplayMetrics().density);
    }
}
