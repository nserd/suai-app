package com.keaoc.nserd.suai_app;

import android.app.Activity;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.keaoc.nserd.suai_app.fragments.MaterialsFragment;
import com.keaoc.nserd.suai_app.fragments.PeopleFragment;
import com.keaoc.nserd.suai_app.fragments.ProfileFragment;
import com.keaoc.nserd.suai_app.fragments.PublicationFragment;
import com.keaoc.nserd.suai_app.fragments.SettingsFragment;
import com.keaoc.nserd.suai_app.fragments.TasksFragment;
import com.keaoc.nserd.suai_app.network.Cookie;
import com.keaoc.nserd.suai_app.network.DataSender;
import com.keaoc.nserd.suai_app.network.NoAutoRedirect;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.keaoc.nserd.suai_app.SuaiAppConstants.ACHIEVEMENTS;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.COOKIES_MODE;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.DISCIPLINES_FRAG;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.DOCUMENTS_FRAG;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.EDUCATIONS_NAMES;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.HOST;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.LIBRARY_FRAG;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.MATERIALS_FRAG;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.PEOPLE_FRAG;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.PROFILE_FRAG;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.PUBLICATION;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.PUBLICATIONS_FRAG;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.PUBLICATIONS_NAMES;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.REPORTS_FRAG;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.SETTINGS_FRAG;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.SP_SETTINGS;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.SUBJECTS;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.TASKS_FRAG;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.USER;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.WORKS;

public class AccountActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final String TAG  = "SUAI.AccountActivity";

    private AppCompatActivity activity;

    public ProgressBar progressBar;
    public ImageView actionBarLogo;

    private Intent pinActivity;

    private boolean initialPin;
    private int pinShowCounter;

    private SQLiteDatabase database;
    private String key;

    private FragmentManager fragmentManager;
    public static String fragmentChangeFlag;

    TabLayout tabs;
    ViewPager viewPager;

    boolean afterOnCreate = false;

    NavigationView navigationView;

    public MenuItem searchItem;

    public String userId;

    public RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        DrawerLayout drawer;

        Toolbar toolbar;

        activity       = this;
        pinActivity    = new Intent(AccountActivity.this, PinCodeActivity.class);
        initialPin     = true;
        pinShowCounter = 2;

        progressBar   = findViewById(R.id.progressActionBar);
        actionBarLogo = findViewById(R.id.actionBarLogo);

        tabs      = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.viewPager);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Log.d(TAG, "onBackStackChanged");
            }
        });

        toolbar         = findViewById(R.id.toolbar);
        drawer          = findViewById(R.id.drawer_layout);
        navigationView  = findViewById(R.id.nav_view);

        afterOnCreate = true;

        //------------------------------------------------------------------------------------------
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new CustomActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        progressBar.getIndeterminateDrawable().
                setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY);
    }

    @Override
    protected void onStart(){
        super.onStart();

        //Защита от бесконечного вызова PinCodeActivity
        if(pinShowCounter > 1) {
            pinActivity.putExtra("initial_pin", initialPin);
            startActivityForResult(pinActivity, 1);

            pinShowCounter = 0;
        }

        pinShowCounter++;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.account_menu, menu);
        searchItem = menu.findItem(R.id.search);
        searchItem.setVisible(false);

        return true;
    }

    /**Загружает с сервера JSON массив данных*/
    private void getDataProfile(){
        final DataSender dataSender;
        queue = Volley.newRequestQueue(this, new NoAutoRedirect());

        dataSender = new DataSender(queue, HOST, activity){
            @Override
            public void doOnAnswer(JSONObject response) {
                super.doOnAnswer(response);

                try {
                    saveAccountData(response);

                    JSONObject user = (JSONObject) response.get("user");
                    getProfileImage(queue, user.get("image").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        final DataSender userIdRequest = new DataSender(queue, HOST, activity){
            @Override
            public void doOnAnswer(String response) {
                super.doOnAnswer(response);

                Map<String, String> resHeaders = getResponseHeaders();

                //TODO: исправить баг (два раза вызывается пин код при смене сессии)
                if(resHeaders.containsKey("Location")) {
                    //erase all data
                    Intent login = new Intent(AccountActivity.this, LoginActivity.class);
                    startActivity(login);
                    finish();
                }

                userId = getUserIdFromResponse(response);
                String getProfileURL = "http://" + HOST + "/getuserprofile/" + userId;

                dataSender.sendJSONRequest(getProfileURL, null, queue, Request.Method.GET);
            }
        };

        showProgressBar();

        String urlForId = "http://" + HOST + "/inside/test";
        userIdRequest.sendStringRequest(urlForId, null, queue, Request.Method.POST);
    }

    /**Загружаент из сети изображение аккаунта*/
    private void getProfileImage(RequestQueue queue, String url){
        if(!url.contains(HOST))
            url = "http://" + HOST + url;

        final CircleImageView profileImage = findViewById(R.id.profileImage);

        DataSender dataSender = new DataSender(queue, HOST, activity){
            @Override
            public void doOnAnswer(Bitmap response) {
                super.doOnAnswer(response);

                Uri uri = saveImageToInternalStorage(response, "ProfileImg");
                profileImage.setImageURI(uri);
            }
        };

        dataSender.sendImageRequest(url, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.RGB_565);
    }

    /** Получение идентификатора пользователя из строки ответа сервера */
    private String getUserIdFromResponse(String response){
        char[] resChar = response.toCharArray();
        StringBuilder userID = new StringBuilder();

        // response example: HOHOHOHOHHOH, user [2639]

        for(int i = 0; i < resChar.length; i++){
            if(resChar[i] == '['){
                for(int j = i + 1; resChar[j] != ']'; j++)
                    userID.append(resChar[j]);

                break;
            }
        }

        return userID.toString();
    }

    /**Получение базы данных для фрагментов*/
    public SQLiteDatabase getDatabase(){
        return database;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (fragmentChangeFlag != null && fragmentChangeFlag.equals(SuaiAppConstants.TABS_FRAGS))
            deleteTabsAndPager();

        switch (id){
            case R.id.nav_profile:
                fragmentChangeFlag = PROFILE_FRAG;
                break;
//            case R.id.nav_publications:
//                fragmentChangeFlag = PUBLICATIONS_FRAG;
//                break;
            case R.id.nav_employees:
                fragmentChangeFlag = PEOPLE_FRAG;
                break;
            case R.id.nav_materials:
                fragmentChangeFlag = MATERIALS_FRAG;
                break;
            case R.id.nav_tasks:
                fragmentChangeFlag = TASKS_FRAG;
                break;
//            case R.id.nav_reports:
//                fragmentChangeFlag = REPORTS_FRAG;
//                break;
//            case R.id.nav_disciplines:
//                fragmentChangeFlag = DISCIPLINES_FRAG;
//                break;
//            case R.id.nav_documents:
//                fragmentChangeFlag = DOCUMENTS_FRAG;
//                break;
//            case R.id.nav_library:
//                fragmentChangeFlag = LIBRARY_FRAG;
//                break;
            case R.id.nav_settings:
                fragmentChangeFlag = SETTINGS_FRAG;
                break;

            case R.id.nav_exit:
                AuxiliaryFunc.eraseAllData(this);
                startActivity(new Intent(AccountActivity.this, LoginActivity.class));
                finish();

                break;
        }

        item.setChecked(true);
        setTitle(item.getTitle());

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    /**Сохраняет все полученные от сервера данные в базу данных*/
    private void saveAccountData(final JSONObject jO){
        JSONObject educations = null;
        JSONObject publications = null;

        try {
            educations = (JSONObject) jO.get("educations");
            publications = (JSONObject) jO.get("publications");
        } catch (Exception e) {

            //TODO: поменять идентификацию студента
            AuxiliaryFunc.eraseAllData(this);
            startActivity(new Intent(AccountActivity.this, LoginActivity.class));

//            AuxiliaryFunc.errorSnackBar(this, "Приложение только для преподавателей");
            finish();
            return;
        }

        Database.DBHelper dbHelper = new Database.DBHelper(this, educations, publications);

        //TODO: убрать вывод ошибки в
        try {
            SQLiteDatabase.loadLibs(activity);
            database = dbHelper.getWritableDatabase(key);
        } catch (SQLiteException e){

            if(e.toString().contains("file is not a database")){
                Log.i(TAG, "Database password changed. Db file delete status: " +
                        Database.deleteDB());

                database = dbHelper.getWritableDatabase(key);
            } else {
                e.printStackTrace();
                return;
            }
        }

        new DatabaseSaver(database, jO).start();
    }

    /**Обработка ответа от PinCodeActivity*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.i(TAG, "onActivityResult in");

        if(resultCode == RESULT_FIRST_USER){
            key = AuxiliaryFunc.aesCipherDEC(this, data.getStringExtra("dbkey"));

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

            getDataProfile();

            showStartFragment();

            initialPin = false;
        }

        if(resultCode == RESULT_CANCELED){
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        }
    }

    private void showStartFragment(){
        //ERROR Can not perform this action after onSaveInstanceState
// В onResult нет доступа к UI (видимо), нужен хэндлер
//        FragmentTransaction ft = fragmentManager.beginTransaction();
//
//        Fragment fragment = ProfileFragment.newInstance(key);
//        ft.replace(R.id.fragments_container, fragment, PROFILE_FRAG);
//        ft.commit();
//
//        fragmentChangeFlag = PROFILE_FRAG;
    }

    protected Uri saveImageToInternalStorage(Bitmap bitmap, String name){
        ContextWrapper wrapper = new ContextWrapper(getApplicationContext());

        File file = wrapper.getDir("Images",MODE_PRIVATE);
        file = new File(file, name +".jpg");

        try{
            OutputStream stream;

            stream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

            stream.flush();
            stream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return Uri.parse(file.getAbsolutePath());
    }

    public void showProgressBar(){
        actionBarLogo.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar(){
        actionBarLogo.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();

            if (fragmentChangeFlag != null && fragmentChangeFlag.equals(SuaiAppConstants.TABS_FRAGS)) {
                deleteTabsAndPager();
            }
        }
    }

    private void deleteTabsAndPager(){
        List<Fragment> fragments = fragmentManager.getFragments();

        if (!fragments.isEmpty()) {
            FragmentTransaction ft = fragmentManager.beginTransaction();

            for (int i = 0; i < fragments.size(); i++) {
                String tag = fragments.get(i).getTag();

                if (tag == null)
                    tag = "empty";

                //открепляет все фрагменты кроме профиля
                if (!tag.equals(PROFILE_FRAG))
                    ft.detach(fragments.get(i));
            }

            ft.commit();
        }

        tabs.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);

        Log.d(TAG, "Tabs deleted.");
    }

    /**Сохраняет данные в фоне*/
    private class DatabaseSaver extends Thread{
        private SQLiteDatabase db;
        private JSONObject     jO;

        DatabaseSaver(SQLiteDatabase db, JSONObject jO){
            this.db = db;
            this.jO = jO;
        }

        @Override
        public void run() {
            super.run();

            try {
                //add objects
                JSONObject user = (JSONObject) jO.get("user");

                Database.addJsonObjectInTable(user, USER, db);

                //add arrays
                JSONArray works        = (JSONArray) user.get(WORKS);
                JSONArray publication  = jO.getJSONArray(PUBLICATION);
                JSONArray achievements = jO.getJSONArray(ACHIEVEMENTS);
                JSONArray subjects     = jO.getJSONArray(SUBJECTS);

                Database.addArrayInTable(works,        WORKS,        db);
                Database.addArrayInTable(publication,  PUBLICATION,  db);
                Database.addArrayInTable(achievements, ACHIEVEMENTS, db);
                Database.addArrayInTable(subjects,     SUBJECTS,     db);

                JSONObject educations = (JSONObject) jO.get("educations");
                JSONObject publications = (JSONObject) jO.get("publications");

                Database.addJsonObjectInTables(educations, db);
                Database.addJsonObjectInTables(publications, db);

                Database.addTableNamesInTable(educations.names(), EDUCATIONS_NAMES, db);
                Database.addTableNamesInTable(publications.names(), PUBLICATIONS_NAMES, db);

                //log
                //Database.logAllTable(WORKS, db);
                //Database.logAllTable(PUBLICATION, db);
                //Database.logAllTable(ACHIEVEMENTS, db);
                //Database.logAllTable(SUBJECTS, db);
                //Database.logAllTable(USER, db);

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        TextView name = findViewById(R.id.name);
                        name.setText(getProfileNameForNavDrawer());

                        hideProgressBar();

                        DrawerLayout drawer = findViewById(R.id.drawer_layout);
                        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String getProfileNameForNavDrawer(){
        StringBuilder name = new StringBuilder();

        String where = "key = 'firstname' OR key = 'lastname'";
        Cursor c = database.query(SuaiAppConstants.USER, new String[] {"key", "value"}, where, null,
                null, null, null);

        c.moveToLast();
        name.append(c.getString(c.getColumnIndex("value")));
        name.append(" ");

        c.moveToFirst();
        name.append(c.getString(c.getColumnIndex("value")));

        c.close();

        return name.toString();
    }

    /**Класс для сработы с фрагментами (для оптимизации)*/
    private class CustomActionBarDrawerToggle extends ActionBarDrawerToggle{
        CustomActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar,
                                    int openDrawerContentDescRes, int closeDrawerContentDescRes) {

            super(activity, drawerLayout, toolbar, openDrawerContentDescRes,
                    closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
            //TODO: оптимизировать --- cursorLoader
            ///....

            if(fragmentChangeFlag != null) {
                Log.d(TAG, "fragmentChangeFlag = " + fragmentChangeFlag);

                Fragment fragment;

                switch (fragmentChangeFlag){
                    case PROFILE_FRAG :
                        fragment = fragmentManager.findFragmentByTag(PROFILE_FRAG);

                        if (fragment == null) {
                            FragmentTransaction ft = fragmentManager.beginTransaction();

                            fragment = ProfileFragment.newInstance(key);
                            ft.replace(R.id.fragments_container, fragment, PROFILE_FRAG);
                            ft.commit();

                        } else { //если фрагмент есть в стаке, но не виден
                            if (fragment.isDetached())
                                fragmentManager.popBackStack();
                        }

                        fragmentChangeFlag = null;

                        break;

                    case PUBLICATIONS_FRAG :
                        fragment = fragmentManager.findFragmentByTag(PUBLICATIONS_FRAG);

                        if (fragment == null) {
                            FragmentTransaction ft = fragmentManager.beginTransaction();

                            fragment = new PublicationFragment();
                            ft.replace(R.id.fragments_container, fragment, PUBLICATIONS_FRAG);
                            ft.commit();
                        }

                        fragmentChangeFlag = null;

                        break;

                    case PEOPLE_FRAG :
                        fragment = fragmentManager.findFragmentByTag(PEOPLE_FRAG);

                        if (fragment == null) {
                            FragmentTransaction ft = fragmentManager.beginTransaction();

                            fragment = new PeopleFragment();
                            ft.replace(R.id.fragments_container, fragment, PEOPLE_FRAG);
                            ft.commit();
                        }

                        fragmentChangeFlag = null;

                        break;

                    case MATERIALS_FRAG :
                        fragment = fragmentManager.findFragmentByTag(MATERIALS_FRAG);

                        if (fragment == null) {
                            FragmentTransaction ft = fragmentManager.beginTransaction();

                            fragment = new MaterialsFragment();
                            ft.replace(R.id.fragments_container, fragment, MATERIALS_FRAG);
                            ft.commit();
                        }

                        fragmentChangeFlag = null;

                        break;

                    case TASKS_FRAG :
                        fragment = fragmentManager.findFragmentByTag(TASKS_FRAG);

                        if (fragment == null) {
                            FragmentTransaction ft = fragmentManager.beginTransaction();

                            fragment = new TasksFragment();
                            ft.replace(R.id.fragments_container, fragment, TASKS_FRAG);
                            ft.commit();
                        }

                        fragmentChangeFlag = null;

                        break;

                    case SETTINGS_FRAG :
                        fragment = fragmentManager.findFragmentByTag(SETTINGS_FRAG);

                        if (fragment == null) {
                            FragmentTransaction ft = fragmentManager.beginTransaction();

                            fragment = new SettingsFragment();
                            ft.replace(R.id.fragments_container, fragment, SETTINGS_FRAG);
                            ft.commit();
                        }

                        fragmentChangeFlag = null;

                        break;
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        SharedPreferences sPref;
        sPref = getSharedPreferences(SP_SETTINGS, MODE_PRIVATE);
        boolean eraseCookie = sPref.getBoolean(COOKIES_MODE, false);

        if(eraseCookie){
            sPref = getSharedPreferences(Cookie.SESSION_COOKIE, MODE_PRIVATE);

            SharedPreferences.Editor editor = sPref.edit();
            editor.clear();
            editor.apply();
        }
    }
}

////TODO: исправить баг (два раза вызывается пин код при смене сессии)
//                if(resHeaders.containsKey("Location")) {
//                        //erase all data
//                        Intent login = new Intent(AccountActivity.this, LoginActivity.class);
//        startActivity(login);
//        finish();
//        }