package com.keaoc.nserd.suai_app.network;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import java.util.Map;

import javax.crypto.Cipher;

import static android.content.Context.MODE_PRIVATE;
import static com.keaoc.nserd.suai_app.AuxiliaryFunc.aesCipherDEC;
import static com.keaoc.nserd.suai_app.AuxiliaryFunc.aesCipherENC;

/**
 * Created by nserd on 14.04.18.
 */
public class Cookie {

    public final static String SESSION_COOKIE = "session_cookie";

    /** Добавление значения Cookie в заголовок запроса */
    public static void addSessionCookie(Map<String, String> headers, AppCompatActivity activity) {
        SharedPreferences sPref;

        sPref = activity.getSharedPreferences(SESSION_COOKIE, MODE_PRIVATE);
        Map<String, ?> cookie = sPref.getAll();

        StringBuilder cookieForHeader = new StringBuilder();

        for(Map.Entry entry : cookie.entrySet()) {
            cookieForHeader.append(entry.getKey());
            cookieForHeader.append("=");

            String decValue = aesCipherDEC(activity, entry.getValue().toString());

            cookieForHeader.append(decValue);
            cookieForHeader.append(";");
        }

        if (cookieForHeader.length() > 0) {
            //удаление последней точки с запятой
            cookieForHeader.delete(cookieForHeader.length() - 1, cookieForHeader.length());

            headers.put("Cookie", cookieForHeader.toString());
        }
    }

    /** Достает индентификатор сессии из заголовка ответа и сохраняет ее значение в cookie */
    public static void getSessionCookie(Map<String, String> headers, AppCompatActivity activity){
        String key = "Set-Cookie";

        if(headers.containsKey(key)){
            SharedPreferences sPref = activity.getSharedPreferences(SESSION_COOKIE, MODE_PRIVATE);
            SharedPreferences.Editor editor = sPref.edit();

            String cookie = headers.get(key);
            String[] splitCookie = cookie.split(";");

            for (String aSplitCookie : splitCookie) {
                String[] buffer = aSplitCookie.split("=");

                if (buffer.length == 2) {
                    String cookieKey = buffer[0];
                    String cookieValue = buffer[1];

                    String encValue = aesCipherENC(activity, cookieValue);

                    editor.putString(cookieKey, encValue);
                    editor.apply();
                }
            }
        }
    }

    public static String getCookieValue(String name, AppCompatActivity activity){
        SharedPreferences sPref;

        sPref = activity.getSharedPreferences(SESSION_COOKIE, MODE_PRIVATE);
        Map<String, ?> cookie = sPref.getAll();

        for(Map.Entry entry : cookie.entrySet())
            if(entry.getKey().equals(name))
                return aesCipherDEC(activity, entry.getValue().toString());

        return null;
    }
}
