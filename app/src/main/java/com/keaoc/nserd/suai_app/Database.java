package com.keaoc.nserd.suai_app;

import android.content.ContentValues;
import android.content.Context;
import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by nserd on 23.05.18.
 */
public class Database {
    private static final String TAG = "Database";
    private static final String DB_NAME = "suai_db";

    //TODO: добавить методы для изъятия данных из таблицы. Создать таблицу с данными юзера
    //TODO: обрабатывать ошибку, когда сессия меняется и бд не разблочить

    static void addArrayInTable(JSONArray array, String nameTable, SQLiteDatabase db){
        ContentValues cv = new ContentValues();

        boolean tableExist = isTableExist(db, nameTable);

        try {
            for (int i = 0; i < array.length(); i++) {
                //put data in row
                if(array.get(i) instanceof  JSONObject) {
                    JSONObject obj = (JSONObject) array.get(i);

                    Iterator<?> keys = obj.keys();

                    while (keys.hasNext()) {
                        String key = (String) keys.next();

                        if (obj.get(key) instanceof String) {
                            if (!key.equals("id")) cv.put(key, (String) obj.get(key));
                            else cv.put("sid", (String) obj.get(key));
                        }
                    }

                } else {
                    //for subjects table
                    cv.put("name", (String) array.get(i));
                }

                //create row in table or update
                if (tableExist) {
//                    String id = (i + 1) + "";
//
//                    int updCount = db.update(nameTable, cv, "id = ?", new String[]{id});
//                    Log.i(TAG + "_" + nameTable, "Rows updated - " + updCount + ", ID = " + id);
                } else {
                    long rowID = db.insert(nameTable, null, cv);
                    Log.i(TAG + "_" + nameTable, "Rows inserted - " + rowID);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    static void addJsonObjectInTable(JSONObject jO, String nameTable, SQLiteDatabase db){
        String keyString = "key";
        String values = "value";

        ContentValues cv = new ContentValues();

        boolean tableExist = isTableExist(db, nameTable);

        try {
            int idCounter = 1;

            Iterator<?> keys = jO.keys();

            while (keys.hasNext()){
                String key = (String) keys.next();

                if(jO.get(key) instanceof String){
                    cv.put(keyString, key);
                    cv.put(values, (String) jO.get(key));

                    if(tableExist){
//                        int updCount = db.update(nameTable, cv, "id = ?", new String[]{idCounter + ""});
//                        Log.i(TAG + "_" + nameTable, "Rows updated - " + updCount + ", ID = " + idCounter);

                    } else {
                        long rowID = db.insert(nameTable, null, cv);
                        Log.i(TAG + "_" + nameTable, "Rows inserted - " + rowID);
                    }
                }

                idCounter++;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    static void addJsonObjectInTables(JSONObject jO, SQLiteDatabase db){
        try{
            JSONArray tableNames = jO.names();

            for (int i = 0; i < tableNames.length(); i++) {
                String tableName = (String)tableNames.get(i);

                JSONObject tableJO = (JSONObject) jO.get(tableName);

                addArrayInTable(tableJO.getJSONArray("data"), tableName, db);
            }

        } catch (JSONException jOE){
            jOE.printStackTrace();
        }
    }

    static void addTableNamesInTable(JSONArray names, String nameTable, SQLiteDatabase db){
        boolean tableExist = isTableExist(db, nameTable);

        try {
            ContentValues cv = new ContentValues();

            for (int i = 0; i < names.length(); i++) {
                cv.put("name", (String) names.get(i));

                if(!tableExist) {
                    long rowID = db.insert(nameTable, null, cv);
                    Log.i(TAG + "_" + nameTable, "Rows inserted - " + rowID);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    static void logAllTable(String tableName, SQLiteDatabase db){
        Cursor c = db.query(tableName, null, null, null, null, null, null);

        //ставим позицию на первую строку выборки, если не строк то false
        if(c.moveToFirst()){
            String[] colNames = c.getColumnNames();

            int[] colIndexes = new int[colNames.length];

            for(int i = 0; i < colIndexes.length; i++)
                colIndexes[i] = c.getColumnIndex(colNames[i]);

            do {
                StringBuilder sb = new StringBuilder();

                for(int i = 0; i < colNames.length; i++) {
                    sb.append(colNames[i]);
                    sb.append(" = ");
                    sb.append(c.getString(colIndexes[i]));
                    sb.append(" ");
                }

                Log.d(TAG + " " + tableName, "\n" + sb.toString() + "\n");

            } while (c.moveToNext());
        }

        c.close();
    }

    private static boolean isTableExist(SQLiteDatabase db, String table){
        Cursor cur = db.query(table, null, null, null, null, null, null);
        boolean exist = cur.getCount() != 0;

        cur.close();

        return exist;
    }

    public static boolean deleteDB(){
        File db = new File("data/data/com.example.nserd.suai_app/databases/" + DB_NAME);
        return db.delete();
    }

    public static class DBHelper extends SQLiteOpenHelper {
        JSONObject educations;
        JSONObject publications;

        public DBHelper(Context context, JSONObject educations, JSONObject publications){
            super(context, DB_NAME, null, 1);

            this.educations = educations;
            this. publications = publications;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.i(TAG, "------ onCreate database ------");

            db.execSQL(SuaiAppConstants.CREATE_WORKS_TABLE);
            db.execSQL(SuaiAppConstants.CREATE_PUBLICATION_TABLE);
            db.execSQL(SuaiAppConstants.CREATE_ACHIEVEMENTS_TABLE);
            db.execSQL(SuaiAppConstants.CREATE_SUBJECTS_TABLE);
            db.execSQL(SuaiAppConstants.CREATE_USER_TABLE);

            db.execSQL(SuaiAppConstants.CREATE_EDUCATIONS_NAMES_TABLE);
            db.execSQL(SuaiAppConstants.CREATE_PUBLICATIONS_NAMES_TABLE);

            createTables(educations, db, SuaiAppConstants.CREATE_EDUCATIONS_TABLE);
            createTables(publications, db, SuaiAppConstants.CREATE_PUBLICATIONS_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }

        private void createTables(JSONObject jO, SQLiteDatabase db, String createSQL){
            try{
                JSONArray tableNames = jO.names();

                for (int i = 0; i < tableNames.length(); i++)
                    db.execSQL("create table " + tableNames.get(i) + createSQL);
            } catch (JSONException jOE){
                jOE.printStackTrace();
            }
        }
    }
}
