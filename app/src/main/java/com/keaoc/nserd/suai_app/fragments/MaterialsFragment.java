package com.keaoc.nserd.suai_app.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.keaoc.nserd.suai_app.AccountActivity;
import com.keaoc.nserd.suai_app.R;
import com.keaoc.nserd.suai_app.network.DataSender;
import com.keaoc.nserd.suai_app.network.NoAutoRedirect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import static com.keaoc.nserd.suai_app.SuaiAppConstants.HOST;

public class MaterialsFragment extends Fragment {

    AccountActivity accountActivity;
    LinearLayout materials_container;

    public MaterialsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_materials, container, false);

        accountActivity = (AccountActivity) getActivity();
        materials_container = view.findViewById(R.id.materialsContainer);

        setRequestMaterials(setJsonParams(accountActivity.userId));
        return view;
    }

    private JSONObject setJsonParams(String userId){
        JSONObject params = new JSONObject();

        try {
            params.put("iduser", userId);
            params.put("semester", 0);
            params.put("subject", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return params;
    }

    private void setRequestMaterials(JSONObject params){
        String url = "http://" + HOST + "/getmaterials/";

        final DataSender dataSender;
        final RequestQueue queue  = accountActivity.queue;

        dataSender = new DataSender(queue, HOST, (AppCompatActivity) getActivity()){
            @Override
            public void doOnAnswer(JSONObject response) {
                super.doOnAnswer(response);

                try {
                    getMaterialsData(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        accountActivity.showProgressBar();
        dataSender.sendJSONRequest(url, params, queue, Request.Method.POST);
    }

    private void getMaterialsData(JSONObject jO) throws JSONException {
        JSONArray materials = jO.getJSONArray("materials");

        for(int i = 0; i < materials.length(); i++){
            if(materials.get(i) instanceof JSONObject) {
                JSONObject item = (JSONObject) materials.get(i);

                String nameStr = item.getString("name");
                String dataStr = item.getString("datecreate");
                String downUrl = item.getString("filelink");

                materials_container.addView(createItem(nameStr, dataStr, downUrl));
            }
        }

        accountActivity.hideProgressBar();
    }

    private View createItem(String nameStr, String dataStr, String url){
        CardView cardView = new CardView(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        params.setMargins(dpToPx(8),dpToPx(8),dpToPx(8), 1);
        cardView.setLayoutParams(params);

        LayoutInflater factory = LayoutInflater.from(getContext());
        View materialsItem = factory.inflate(R.layout.item_materials, null);

        TextView name = materialsItem.findViewById(R.id.materialsName);
        TextView data = materialsItem.findViewById(R.id.materialsDate);

        Button download = materialsItem.findViewById(R.id.materialsDownload);
        ProgressBar progressBar = materialsItem.findViewById(R.id.materials_progress);
        ConstraintLayout progressLayout = materialsItem.findViewById(R.id.progressLayout);

        download.setOnClickListener(new DownloadClickListener(download, progressBar, progressLayout,
                url));

        name.setText(nameStr);
        data.setText(dataStr);

        cardView.addView(materialsItem);

        return cardView;
    }

    private int dpToPx(int dp){
        return (int) (dp * getContext().getResources().getDisplayMetrics().density);
    }

    class DownloadClickListener implements View.OnClickListener {
        String url;
        Button button;
        ProgressBar progressBar;
        ConstraintLayout progressLayout;

        DownloadClickListener(Button button, ProgressBar progressBar, ConstraintLayout progressLayout,
                              String url){
            this.url = "http://" + HOST + url;
            this.button = button;
            this.progressBar = progressBar;
            this.progressLayout = progressLayout;

            progressBar.setIndeterminate(true);
        }

        @Override
        public void onClick(View v) {
            button.setVisibility(View.GONE);
            progressLayout.setVisibility(View.VISIBLE);

            RequestQueue queue = Volley.newRequestQueue(getContext(), new NoAutoRedirect());
            DataSender dataSender = new DataSender(queue, HOST, accountActivity){
                @Override
                public void doOnAnswer(byte[] response) {
                    super.doOnAnswer(response);

                    Map<String, String> resHeaders = getResponseHeaders();
                    String cD = resHeaders.get("Content-Disposition");

                    String fileName = parseContentDisposition(cD);

                    if (response!=null) {
                        try {
                            File fileDir = new File(accountActivity.getFilesDir(), "documents");
                            fileDir.mkdir();

                            final File document = new File(fileDir, fileName);

                            FileOutputStream outputStream = new FileOutputStream(document);
                            outputStream.write(response);
                            outputStream.close();

                            progressLayout.setVisibility(View.GONE);
                            button.setVisibility(View.VISIBLE);

                            Snackbar snackbar = Snackbar.make(materials_container, "Файл загружен.", Snackbar.LENGTH_LONG);
                            snackbar.setAction("открыть", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Uri uri = FileProvider.getUriForFile(getContext(), "com.example.nserd.suai_app.fileprovider", document);
                                    String mime = accountActivity.getContentResolver().getType(uri);

                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setDataAndType(uri, mime);
                                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    startActivity(intent);
                                }
                            });

                            snackbar.show();

                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            };

            dataSender.sendFileRequest(url, null, queue, Request.Method.GET);
        }

        private String parseContentDisposition(String cD){
            String out = null;

            String[] buff = cD.split(";");

            for(int i = 0; i < buff.length; i++){
                if(buff[i].contains("filename")){
                    String[] buff2 = buff[i].split("\"");
                    out = buff2[1];
                    break;
                }
            }

            return out;
        }
    }
}
