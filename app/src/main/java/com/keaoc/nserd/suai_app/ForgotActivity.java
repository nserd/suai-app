package com.keaoc.nserd.suai_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.keaoc.nserd.suai_app.network.DataSender;
import com.keaoc.nserd.suai_app.network.NoAutoRedirect;

import java.util.HashMap;
import java.util.Map;

import static com.keaoc.nserd.suai_app.SuaiAppConstants.HOST;

public class ForgotActivity extends AppCompatActivity {

    private AppCompatActivity activity;
    private Intent loadingActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        activity = this;
        loadingActivity = new Intent(ForgotActivity.this, LoadingActivity.class);
    }

    public void onClick(View v){
        if(v.getId() == R.id.button_send_forgot){
            EditText emailText = findViewById(R.id.email_forgot);

            String email = emailText.getText().toString();

            if(email.length() == 0){
                Toast.makeText(getApplicationContext(), "Поле ввода email не заполнено",
                        Toast.LENGTH_SHORT).show();
            }
            else{
                //TODO: почитать про подгон элементов экрана под клавиатуру
                startActivity(loadingActivity);
                sendDataOnServer(email);
            }
        }
    }

    private void sendDataOnServer(String data){
        String url = "http://" + HOST + "/user/forgot-password";

        RequestQueue queue = Volley.newRequestQueue(activity, new NoAutoRedirect());
        DataSender dataSender = new DataSender(queue, HOST, activity){
            @Override
            public void doOnAnswer(String response) {
                super.doOnAnswer(response);

                Map<String, String> resHeaders = getResponseHeaders();
                if(resHeaders.containsKey("Location")) {
                    Toast.makeText(getApplicationContext(),
                            "На вашу электронную почту выслана инструкция по восстановлению пароля",
                            Toast.LENGTH_LONG).show();

                    LoadingActivity.loadingActivity.finish();
                    finish();
                }
                else{
                    LoadingActivity.loadingActivity.finish();
                    AuxiliaryFunc.errorSnackBar(activity,
                            "Пользователя с такой почтой не существует");
                }
            }

            @Override
            public void doOnError(VolleyError error) {
                super.doOnError(error);

                assert error.networkResponse != null;
                Toast.makeText(activity.getApplicationContext(),
                        "Ошибка. Код " + error.networkResponse.statusCode, Toast.LENGTH_LONG).show();

                LoadingActivity.loadingActivity.finish();
            }
        };

        Map<String, String> param = new HashMap<>();
        param.put("email", data);

        dataSender.sendStringRequest(url, param, queue, Request.Method.POST);
    }
}
