package com.keaoc.nserd.suai_app.network;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/** Created by nserd on 15.04.18. */

public class DataSender {
    private final String TAG;
    private final String HOST;

    private RequestQueue queue;

    private final AppCompatActivity activity;

    private Map<String, String> responseHeaders;

    private String url;

    public DataSender(RequestQueue queue, String HOST, AppCompatActivity activity){
        TAG = "SUAI.DataSender";

        this.HOST  = HOST;
        this.queue = queue;

        this.activity = activity;

        url = null;
    }

    public void sendStringRequest(final String url, final Map<String, String> params,
                                  final RequestQueue queue, final int method){
        this.url = url;

        StringRequest strReq;

        Response.Listener<String> resListener;
        Response.ErrorListener resErrorListener;

        resListener = new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                doOnAnswer(response);
            }
        };

        resErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Response error: " + error.toString());
                doOnError(error);
            }
        };

        //Формирование строкового запроса
        strReq = new StringRequest(method, url, resListener, params, resErrorListener){
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = super.getHeaders();

                headers.put("Host", HOST);

                Cookie.addSessionCookie(headers, activity);

                Log.d(TAG, "to URL: " + url);
                logHeaderAndParams("Request", headers, params);

                return headers;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                responseHeaders = response.headers;
                Cookie.getSessionCookie(responseHeaders, activity);

                logHeaderAndParams("Response", responseHeaders, null);

                return super.parseNetworkResponse(response);
            }
        };

        queue.add(strReq);
    }

    public void sendJSONRequest(final String url, final JSONObject params,
                                final RequestQueue queue, final int method){
        this.url = url;

        JsonObjectRequest jsonObjReq;

        Response.Listener<JSONObject> resListener;
        Response.ErrorListener resErrorListener;

        resListener = new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                doOnAnswer(response);
            }
        };

        resErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
                doOnError(error);
            }
        };

        jsonObjReq = new JsonObjectRequest(method, url, params, resListener, resErrorListener){
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<>();

                headers.put("User-agent", System.getProperty("http.agent"));
                headers.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                headers.put("Accept-Language", "en-US,en;q=0.5");
                headers.put("Content-Type", "application/x-www-form-urlencoded");

                Cookie.addSessionCookie(headers, activity);

                headers.put("Connection", "keep-alive");
                headers.put("Upgrade-Insecure-Requests", "1");

                Log.d(TAG, "to URL: " + url);
                logHeaderAndParams("Request", headers, null);

                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                responseHeaders = response.headers;
                Cookie.getSessionCookie(responseHeaders, activity);

                logHeaderAndParams("Response", responseHeaders, null);

                return super.parseNetworkResponse(response);
            }
        };

        queue.add(jsonObjReq);
    }

    public void sendFileRequest(final String url, final Map<String, String> params,
                                  final RequestQueue queue, final int method){
        this.url = url;

        FileDownloader fileReq;

        Response.Listener<byte[]> resListener;
        Response.ErrorListener resErrorListener;

        resListener = new Response.Listener<byte[]>(){
            @Override
            public void onResponse(byte[] response) {
                doOnAnswer(response);
            }
        };

        resErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Response error: " + error.toString());
                doOnError(error);
            }
        };

        //Формирование строкового запроса
        fileReq = new FileDownloader(method, url, resListener, params, resErrorListener){
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<>();

                headers.put("User-agent", System.getProperty("http.agent"));
                headers.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                headers.put("Accept-Language", "en-US,en;q=0.5");
                headers.put("Content-Type", "application/x-www-form-urlencoded");

                Cookie.addSessionCookie(headers, activity);

                headers.put("Connection", "keep-alive");
                headers.put("Upgrade-Insecure-Requests", "1");

                Log.d(TAG, "to URL: " + url);
                logHeaderAndParams("Request", headers, null);

                return headers;
            }

            @Override
            protected Response<byte[]> parseNetworkResponse(NetworkResponse response) {
                responseHeaders = response.headers;
                Cookie.getSessionCookie(responseHeaders, activity);

                logHeaderAndParams("Response", responseHeaders, null);

                return super.parseNetworkResponse(response);
            }
        };

        queue.add(fileReq);
    }

    public void sendImageRequest(String url, ImageView.ScaleType scaleType, Bitmap.Config config){
        this.url = url;

        ImageRequest imageRequest = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        doOnAnswer(response);
                    }
                },
                0,  // Image width
                0, // Image height
                scaleType,    // Image scale type
                config,        //Image decode configuration
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );

        queue.add(imageRequest);
    }

    protected void redirect(RequestQueue queue){
        String redirectURL = responseHeaders.get("Location");

        if(!redirectURL.contains(HOST))
            redirectURL = "http://" + HOST + redirectURL;

        sendStringRequest(redirectURL, null, queue, Request.Method.GET);
    }

    public void doOnAnswer(String response){
        Log.d(TAG, "String Response: " + response);

        //overwrite code...
    }

    public void doOnAnswer(JSONObject response){
        Log.d(TAG, "JSON Response: " + response.toString());

        //overwrite code...
    }

    public void doOnAnswer(Bitmap response){
        //overwrite code...
    }

    public void doOnAnswer(byte[] response){
        //overwrite code...
    }

    public void doOnError(VolleyError error){
        //overwrite code...
    }

    protected Map<String, String> getResponseHeaders(){
        return responseHeaders;
    }

    protected String getURL(){
        return url;
    }

    private void logHeaderAndParams(String name, Map<String, String> headers, Map<String, String> params){
        StringBuilder sb = new StringBuilder("  \n" + name + " headers: \n");

        for(Map.Entry entry : headers.entrySet()) {
            sb.append("Key: ");
            sb.append(entry.getKey());
            sb.append(" Value: ");
            sb.append(entry.getValue());
            sb.append("\n");
        }

        sb.append("\n");

        if(params != null) {
            sb.append("Params:");
            sb.append("\n");

            for (Map.Entry entry : params.entrySet()) {
                sb.append("Key: ");
                sb.append(entry.getKey());
                sb.append(" Value: ");
                sb.append(entry.getValue());
                sb.append("\n");
            }
        }

        Log.d(TAG, sb.toString());
    }
}