package com.keaoc.nserd.suai_app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.keaoc.nserd.suai_app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PublicationFragment extends Fragment {
    public PublicationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_publicaton, container, false);
    }
}
