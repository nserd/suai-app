package com.keaoc.nserd.suai_app.fragments.tabs;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.keaoc.nserd.suai_app.AccountActivity;
import com.keaoc.nserd.suai_app.R;
import com.keaoc.nserd.suai_app.SuaiAppConstants;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PublicationsFragment extends Fragment {
    LinearLayout pubContainer;

    public PublicationsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_publications, container, false);

        AccountActivity accountActivity = (AccountActivity) getActivity();

        pubContainer = view.findViewById(R.id.pub_container);

        setDataInViews(accountActivity.getDatabase());

        return view;
    }

    private void setDataInViews(SQLiteDatabase db){
        ArrayList<String> tableNames = getTablesNames(db);

        for(String name : tableNames){
            CardView cardView     = new CardView(getContext());
            LinearLayout mainItem = new LinearLayout(getContext());
            TextView title        = new TextView(getContext());

            cardView.setLayoutParams(setupLayout(8, 8, 8, 8, -1, true));

            mainItem.setOrientation(LinearLayout.VERTICAL);
            mainItem.setDividerDrawable(getContext().getResources().
                    getDrawable(R.drawable.divider_large));
            mainItem.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);

            title.setLayoutParams(setupLayout(8, 8, 8, 8, -1, true));
            title.setTypeface(Typeface.DEFAULT_BOLD);

            title.setText(getPubName(name, db));
            title.setTextColor(getContext().getResources().getColor(R.color.suaiBlack));

            mainItem.addView(title);
            mainItem.addView(getTableData(name,db));

            cardView.addView(mainItem);

            pubContainer.addView(cardView);
        }
    }

    private String getPubName(String tableName, SQLiteDatabase db){
        String eduName;

        Cursor c = db.query(tableName, new String[]{"pub_trans_name"},
                null, null, null, null, null);

        c.moveToFirst();
        eduName = c.getString(0);
        c.close();

        return eduName;
    }

    private LinearLayout createRow(String s1, String s2){
        LinearLayout row = new LinearLayout(getContext());
        row.setOrientation(LinearLayout.VERTICAL);
        row.setLayoutParams(setupLayout(1, 1, 1, 1, -1, true));

        if(s1.equals("Название:")){
            TextView title = new TextView(getContext());
            title.setLayoutParams(setupLayout(8, 1, 8, 1, -1, true));
            title.setTextColor(getContext().getResources().getColor(R.color.sDarkBlue2));
            title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

            title.setText(s2);
            row.addView(title);
        }
        else {
            TextView col1 = new TextView(getContext());
            col1.setLayoutParams(setupLayout(8, 1, 8, 1, -1, true));
            col1.setTextColor(getContext().getResources().getColor(R.color.suaiBlack));

            TextView col2 = new TextView(getContext());
            col2.setLayoutParams(setupLayout(8, 1, 8, 8, -1, true));

            col1.setText(s1);
            col2.setText(s2);

            row.addView(col1);
            row.addView(col2);
        }

        return row;
    }

    private LinearLayout getTableData(String tableName, SQLiteDatabase db){
        Cursor c = db.query(tableName, new String[]{"title, authors, pagescount, publishing_office, " +
                        "isbn, magazineName, pages, conference, year"},
                null, null, null, null, null);

        c.moveToFirst();

        int indexTitle = c.getColumnIndex("title");
        int indexAuthors = c.getColumnIndex("authors");
        int indexPagesC = c.getColumnIndex("pagescount");
        int indexPubOffice = c.getColumnIndex("publishing_office");
        int indexIsbn = c.getColumnIndex("isbn");
        int indexMagazine = c.getColumnIndex("magazineName");
        int indexPages = c.getColumnIndex("pages");
        int indexConf = c.getColumnIndex("pages");
        int indexYear = c.getColumnIndex("year");

        LinearLayout category = new LinearLayout(getContext());
        category.setOrientation(LinearLayout.VERTICAL);
        category.setDividerDrawable(getContext().getResources().
                getDrawable(R.drawable.divider));
        category.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);

        do{
            LinearLayout description = new LinearLayout(getContext());
            description.setOrientation(LinearLayout.VERTICAL);

            addDataInView(description, c, "Название:",            indexTitle);
            addDataInView(description, c, "Авторы:",              indexAuthors);
            addDataInView(description, c, "Объем (страниц):",     indexPagesC);
            addDataInView(description, c, "Издательство:",        indexPubOffice);
            addDataInView(description, c, "ISBN:",                indexIsbn);
            addDataInView(description, c, "Сборник/Журнал:",      indexMagazine);
            addDataInView(description, c, "Страницы в сборнике:", indexPages);
            addDataInView(description, c, "Конференция:",         indexConf);
            addDataInView(description, c, "Год:",                 indexYear);

            category.addView(description);
        } while (c.moveToNext());
        c.close();

        return category;
    }

    private void addDataInView(LinearLayout parent, Cursor c, String name, int index){
        if(c.getString(index).length() > 0)
            parent.addView(createRow(name, c.getString(index)));
    }

    private ArrayList<String> getTablesNames(SQLiteDatabase db){
        ArrayList<String> tableNames = new ArrayList<>();

        Cursor c = db.query(SuaiAppConstants.PUBLICATIONS_NAMES, new String[]{"name"},
                null, null, null, null, null);

        c.moveToFirst();

        int index = c.getColumnIndex("name");

        do tableNames.add(c.getString(index));
        while (c.moveToNext());

        c.close();

        return tableNames;
    }

    private LayoutParams setupLayout(int l, int t, int r, int b, float weight, boolean mode){
        LayoutParams viewParams;

        viewParams = mode
                ? new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                : new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        if(weight != -1)
            viewParams.weight = weight;

        viewParams.setMargins(dpToPx(l),dpToPx(t),dpToPx(r),dpToPx(b));

        return viewParams;
    }

    private int dpToPx(int dp){
        return (int) (dp * getContext().getResources().getDisplayMetrics().density);
    }
}
