package com.keaoc.nserd.suai_app.fragments.tabs;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.keaoc.nserd.suai_app.AccountActivity;
import com.keaoc.nserd.suai_app.R;
import com.keaoc.nserd.suai_app.SuaiAppConstants;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class AchievementsFragment extends Fragment {
    LinearLayout linearLayout;

    public AchievementsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_achievements, container, false);

        AccountActivity accountActivity = (AccountActivity) getActivity();
        linearLayout = view.findViewById(R.id.achContainer);
        linearLayout.setDividerDrawable(getResources().getDrawable(R.drawable.divider));
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        
        setDataInViews(accountActivity.getDatabase());

        return view;
    }

    private void setDataInViews(SQLiteDatabase db) {
        Cursor c = db.query(SuaiAppConstants.ACHIEVEMENTS, new String[]{"text"},
                null, null, null, null, null);

        c.moveToFirst();

        int index = c.getColumnIndex("text");

        if (!c.isNull(index)) {
            linearLayout.removeAllViews();

            LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            int px = (int) dpToPx(8);

            viewParams.setMargins(px, px, px, px);

            do {
                String achText = c.getString(index);

                TextView textView = new TextView(getContext());
                textView.setLayoutParams(viewParams);
                textView.setTextColor(getContext().getResources().getColor(R.color.suaiBlack));

                textView.setText(achText);
                linearLayout.addView(textView);

            } while (c.moveToNext());
        }
        c.close();
    }

    private float dpToPx(int dp){
        return dp * getContext().getResources().getDisplayMetrics().density;
    }
}