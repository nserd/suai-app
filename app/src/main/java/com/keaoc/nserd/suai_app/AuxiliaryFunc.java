package com.keaoc.nserd.suai_app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.keaoc.nserd.suai_app.network.Cookie;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import static android.content.Context.MODE_PRIVATE;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.SP_PIN;

/** Created by nserd on 12.05.18 */
public class AuxiliaryFunc {
    private static final String TAG = "SUAI.AuxiliaryFunc";

    /** Запись в локальное хранилище */
    public static void setPreference(String name, String data, AppCompatActivity a){
        SharedPreferences sPref;
        SharedPreferences.Editor editor;

        sPref = a.getPreferences(MODE_PRIVATE);

        editor = sPref.edit();
        editor.putString(name, data);
        editor.apply();

        Log.i(TAG, "Set shared preference: " + data);
    }

    /** Получение данных из локального хранилища */
    public static String getPreference(String name, AppCompatActivity a){
        SharedPreferences sPref;
        String data;

        sPref = a.getPreferences(MODE_PRIVATE);

        data = sPref.getString(name, "");

        Log.i(TAG, "Get shared preference: " + data);

        return data;
    }

    /**Вывод снэкбара с сообщением об ошибке*/
    public static void errorSnackBar(AppCompatActivity activity, String text){
        Snackbar snackbar = Snackbar.make(activity.findViewById(R.id.constraintLayout3),
                text, Snackbar.LENGTH_LONG);

        View snackbarView = snackbar.getView();

        snackbarView.setBackgroundColor(activity.getResources().getColor(R.color.sRed1));

        snackbar.show();
    }

    /**Создание уникального идентификатора*/
    public static String getCustomID(Context context){
        StringBuilder deviceData = new StringBuilder();

        if(!Build.BOARD.contains(Build.UNKNOWN))        deviceData.append(Build.BOARD);
        if(!Build.BRAND.contains(Build.UNKNOWN))        deviceData.append(Build.BRAND);
        if(!Build.DEVICE.contains(Build.UNKNOWN))       deviceData.append(Build.DEVICE);
        if(!Build.DISPLAY.contains(Build.UNKNOWN))      deviceData.append(Build.DISPLAY);
        if(!Build.HOST.contains(Build.UNKNOWN))         deviceData.append(Build.HOST);
        if(!Build.ID.contains(Build.UNKNOWN))           deviceData.append(Build.ID);
        if(!Build.MANUFACTURER.contains(Build.UNKNOWN)) deviceData.append(Build.MANUFACTURER);
        if(!Build.MODEL.contains(Build.UNKNOWN))        deviceData.append(Build.MODEL);
        if(!Build.PRODUCT.contains(Build.UNKNOWN))      deviceData.append(Build.PRODUCT);
        if(!Build.TAGS.contains(Build.UNKNOWN))         deviceData.append(Build.TAGS);
        if(!Build.TYPE.contains(Build.UNKNOWN))         deviceData.append(Build.TYPE);
        if(!Build.USER.contains(Build.UNKNOWN))         deviceData.append(Build.USER);

        if(getAndroidID(context) != null) deviceData.append(getAndroidID(context));
        if(getMacAddress(context) != null) deviceData.append(getMacAddress(context));

        if(!deviceData.toString().equals(""))
            return sha256(deviceData.toString());
        else
            return null;
    }

    //TODO: проверить правильность работы
    private static String byteToHexString(byte[] bytes){
        StringBuilder hexString = new StringBuilder();

        for (byte aHash : bytes) {
            String hex = Integer.toHexString(0xff & aHash);

            if (hex.length() == 1) hexString.append('0');

            hexString.append(hex);
        }

        return hexString.toString();
    }

    @SuppressLint("HardwareIds")
    private static String getMacAddress(Context context){
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().
                getSystemService(Context.WIFI_SERVICE);

        if(wifiManager == null)
            return null;

        WifiInfo wInfo = wifiManager.getConnectionInfo();
        return wInfo.getMacAddress();
    }

    @SuppressLint("HardwareIds")
    private static String getAndroidID(Context context){
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));

            return byteToHexString(hash);

        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    private static byte[] hexStringToByte(String hexString){
        int len = hexString.length() / 2;
        byte[] result = new byte[len];

        for(int i = 0; i < len; i++)
            result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2), 16).byteValue();

        return result;
    }

    public static String aesCipherENC(Context context, String data){
        try {
            byte[] buff = AuxiliaryFunc.getCustomID(context).getBytes();
            byte[] key = new byte[buff.length / 2];

            System.arraycopy(buff, 0, key, 0, key.length);

            SecretKey secretKey = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            byte[] rawData = cipher.doFinal(data.getBytes());

            return byteToHexString(rawData);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String aesCipherDEC(Context context, String data){
        try {
            byte[] buff = AuxiliaryFunc.getCustomID(context).getBytes();
            byte[] key = new byte[buff.length / 2];

            System.arraycopy(buff, 0, key, 0, key.length);

            SecretKey secretKey = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            byte[] rawData = cipher.doFinal(hexStringToByte(data));

            return new String(rawData);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**Отчищает все данные о пользователе из приложения (выход из аккаунта)*/
    public static boolean eraseAllData(Context context){
        boolean isDeleted = Database.deleteDB();

        SharedPreferences sPref_Pin;
        SharedPreferences sPref_Cookie;

        sPref_Pin = context.getSharedPreferences(SP_PIN, MODE_PRIVATE);
        SharedPreferences.Editor editor = sPref_Pin.edit();
        editor.clear();
        editor.apply();

        sPref_Cookie = context.getSharedPreferences(Cookie.SESSION_COOKIE, MODE_PRIVATE);
        editor = sPref_Cookie.edit();
        editor.clear();
        editor.apply();

        if(isDeleted)
            Log.i(TAG, "All data deleted.");
        else
            Log.e(TAG, "Data is partially deleted.");

        return isDeleted;
    }
}
