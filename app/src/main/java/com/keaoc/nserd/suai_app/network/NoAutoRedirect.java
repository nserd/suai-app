package com.keaoc.nserd.suai_app.network;

import com.android.volley.toolbox.HurlStack;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/** Created by nserd on 31.03.18. */
public class NoAutoRedirect extends HurlStack {
    @Override
    protected HttpURLConnection createConnection(URL url) throws IOException {
        HttpURLConnection connection = super.createConnection(url);

        //отключения автоматического редиректа
        connection.setInstanceFollowRedirects(false);

        return connection;
    }
}
