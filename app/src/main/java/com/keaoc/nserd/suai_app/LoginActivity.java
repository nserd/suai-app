package com.keaoc.nserd.suai_app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import android.net.ConnectivityManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import com.keaoc.nserd.suai_app.network.Cookie;
import com.keaoc.nserd.suai_app.network.DataSender;
import com.keaoc.nserd.suai_app.network.NoAutoRedirect;

import java.util.HashMap;
import java.util.Map;

import static com.keaoc.nserd.suai_app.AuxiliaryFunc.aesCipherENC;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.HOST;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.SP_SAVED_LOGIN;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "SUAI.LoginActivity";

    private AppCompatActivity activity;

    private EditText loginEditText;
    private EditText passEditText;

    private ImageView imageView;

    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        activity = this;

        loginEditText = findViewById(R.id.username);
        passEditText  = findViewById(R.id.password);
        imageView     = findViewById(R.id.suaiLogo);

        loginEditText.setText(AuxiliaryFunc.getPreference(SP_SAVED_LOGIN, this));

        //------костыль-для-сервера------
        SharedPreferences sPref = getSharedPreferences(Cookie.SESSION_COOKIE, MODE_PRIVATE);
        Editor editor = sPref.edit();

        String encValue = aesCipherENC(activity, "initial_cookie");

        editor.putString("PHPSESSID", encValue);
        editor.apply();
        //-------------------------------

        if(!isNetworkConnected()){
            Toast.makeText(getApplicationContext(),
                    "Нет соединения с интернетом", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        //отчистка куки после завершения работы приложения
//        SharedPreferences sPref = getSharedPreferences(Cookie.SESSION_COOKIE, MODE_PRIVATE);
//        Editor editor = sPref.edit();
//
//        editor.clear();
//        editor.apply();
    }

    @Override
    protected void onResume(){
        super.onResume();

        if(queue != null)
            queue.cancelAll(new RequestQueue.RequestFilter() {
                @Override
                public boolean apply(Request<?> request) {
                    return true;
                }
            });
    }

    /** Обработка нажатий на элементы Activity */
    public void onClick(View v){
        Intent loading = new Intent(LoginActivity.this, LoadingActivity.class);

        switch (v.getId()){
            case R.id.suaiLogo:
                Animation anim;

                anim = AnimationUtils.loadAnimation(this, R.anim.anim_scale);
                v.startAnimation(anim);

                break;

            case R.id.loginButton :
                AuxiliaryFunc.setPreference(SP_SAVED_LOGIN, loginEditText.getText().toString(), this);

                if(isNetworkConnected()) {
                    String username = loginEditText.getText().toString();
                    String password = passEditText.getText().toString();

                    if(username.length() == 0 || password.length() == 0){
                        Toast.makeText(getApplicationContext(),
                                "Не все поля заполнены", Toast.LENGTH_SHORT).show();
                    }

                    else {
                        authentication(username, password);
                        startActivity(loading);
                    }
                }

                else {
                    Toast.makeText(getApplicationContext(),
                            "Нет соединения с интернетом", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.forgotPassButton :
                Intent forgotActivity = new Intent(LoginActivity.this, ForgotActivity.class);
                startActivity(forgotActivity);

                break;
        }
    }

    /** Отправка группы запросов для осуществления аутентификации на сервере */
    private void authentication(final String username, String password){
        final String urlLoginCheck = "http://" + HOST + "/user/login_check";
        final String urlLogin      = "http://" + HOST + "/user/login";

        Map<String, String> params = new HashMap<>();

        queue = Volley.newRequestQueue(this, new NoAutoRedirect());

        params.put("_username", username);
        params.put("_password", password);

        DataSender authRequest = new DataSender(queue, HOST, activity){
            @Override
            public void doOnAnswer(String response) {
                super.doOnAnswer(response);

                Map<String, String> resHeaders = getResponseHeaders();

                if(resHeaders.containsKey("Location")) {
                    if(resHeaders.get("Location").equals(urlLogin)) {
                        LoadingActivity.loadingActivity.finish();
                        AuxiliaryFunc.errorSnackBar(activity, "Неверный логин или пароль");
                    }
                    else
                        redirect(queue);
                }
                else {
                    Intent accountActivity = new Intent(LoginActivity.this, AccountActivity.class);
                    startActivity(accountActivity);

                    Log.i(TAG, "Logged in");

                    finish();
                }
            }

            @Override
            public void doOnError(VolleyError error){
                super.doOnError(error);

                assert error.networkResponse != null;
                Toast.makeText(activity.getApplicationContext(),
                        "Ошибка. Код " + error.networkResponse.statusCode, Toast.LENGTH_LONG).show();

                LoadingActivity.loadingActivity.finish();
            }
        };

        authRequest.sendStringRequest(urlLoginCheck, params, queue, Request.Method.POST);
    }

    /** Проверка подключения к интернету */
    //выдает тост только если на телефоне отключен интернет.
    // TODO: добавить оповещение о отсутствия связи.
    private boolean isNetworkConnected(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return  cm.getActiveNetworkInfo() != null;
    }
}