package com.keaoc.nserd.suai_app.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.keaoc.nserd.suai_app.R;

import static com.keaoc.nserd.suai_app.SuaiAppConstants.COOKIES_MODE;
import static com.keaoc.nserd.suai_app.SuaiAppConstants.SP_SETTINGS;

public class SettingsFragment extends Fragment {
    SharedPreferences settingsSP;

    Switch cookieSwitch;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        settingsSP = getActivity().getSharedPreferences(SP_SETTINGS, Context.MODE_PRIVATE);

        cookieSwitch = rootView.findViewById(R.id.cookieSwitch);

        setupSettings();

        cookieSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = settingsSP.edit();
                editor.putBoolean(COOKIES_MODE, isChecked);
                editor.apply();
            }
        });

        return rootView;
    }

    private void setupSettings(){
        boolean cookiesMode = settingsSP.getBoolean(COOKIES_MODE, false);
        cookieSwitch.setChecked(cookiesMode);
    }
}
