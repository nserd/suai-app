package com.keaoc.nserd.suai_app.fragments.tabs;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.keaoc.nserd.suai_app.AccountActivity;
import com.keaoc.nserd.suai_app.R;
import com.keaoc.nserd.suai_app.SuaiAppConstants;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EducationsFragment extends Fragment {
    LinearLayout eduContainer;

    public EducationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_education, container, false);

        AccountActivity accountActivity = (AccountActivity) getActivity();

        eduContainer = view.findViewById(R.id.edu_container);

        setDataInViews(accountActivity.getDatabase());

        return view;
    }

    private void setDataInViews(SQLiteDatabase db){
        ArrayList<String> tableNames = getTablesNames(db);

        for(String name : tableNames){
            CardView cardView     = new CardView(getContext());
            LinearLayout mainItem = new LinearLayout(getContext());
            TextView title        = new TextView(getContext());

            cardView.setLayoutParams(setupLayout(8, 8, 8, 8, -1, true));

            mainItem.setOrientation(LinearLayout.VERTICAL);
            mainItem.setDividerDrawable(getContext().getResources().
                    getDrawable(R.drawable.divider_large));
            mainItem.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);

            title.setLayoutParams(setupLayout(8, 8, 8, 8, -1, true));
            title.setTypeface(Typeface.DEFAULT_BOLD);

            title.setText(getEduName(name, db));
            title.setTextColor(getContext().getResources().getColor(R.color.suaiBlack));

            mainItem.addView(title);
            mainItem.addView(getTableData(name,db));

            cardView.addView(mainItem);

            eduContainer.addView(cardView);
        }
    }

    private String getEduName(String tableName, SQLiteDatabase db){
        String eduName;

        Cursor c = db.query(tableName, new String[]{"edu_name"},
                null, null, null, null, null);

        c.moveToFirst();
        eduName = c.getString(0);
        c.close();

        return eduName;
    }

    private LinearLayout createRow(String s1, String s2){
        LinearLayout row = new LinearLayout(getContext());
        row.setOrientation(LinearLayout.HORIZONTAL);
        row.setLayoutParams(setupLayout(1, 1, 1, 1, -1, true));

        TextView col1 = new TextView(getContext());
        col1.setLayoutParams(setupLayout(8, 1, 8, 1, 1, true));
        col1.setTextColor(getContext().getResources().getColor(R.color.suaiBlack));

        TextView col2 = new TextView(getContext());
        col2.setLayoutParams(setupLayout(8, 1, 8, 8, 1, true));

        col1.setText(s1);
        col2.setText(s2);

        row.addView(col1);
        row.addView(col2);

        return row;
    }

    private LinearLayout getTableData(String tableName, SQLiteDatabase db){
        Cursor c = db.query(tableName, new String[]{"country, city, " +
                        "speciality, institute, department, chair, edu_form_name, " +
                        "edu_qualification_name, year"},
                null, null, null, null, null);

        c.moveToFirst();

        int indexCountry = c.getColumnIndex("country");
        int indexCity = c.getColumnIndex("city");
        int indexSpeciality = c.getColumnIndex("speciality");
        int indexInstitute = c.getColumnIndex("institute");
        int indexDepartment = c.getColumnIndex("department");
        int indexChair = c.getColumnIndex("chair");
        int indexEduFormName = c.getColumnIndex("edu_form_name");
        int indexEduQualifName = c.getColumnIndex("edu_qualification_name");
        int indexYear = c.getColumnIndex("year");

        LinearLayout table = new LinearLayout(getContext());
        table.setOrientation(LinearLayout.VERTICAL);
        table.setDividerDrawable(getContext().getResources().getDrawable(R.drawable.divider));
        table.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);

        do{
            LinearLayout description = new LinearLayout(getContext());
            description.setOrientation(LinearLayout.VERTICAL);

            addDataInView(description, c, "Страна:",         indexCountry);
            addDataInView(description, c, "Город:",          indexCity);
            addDataInView(description, c, "Специальность:",  indexSpeciality);
            addDataInView(description, c, "ВУЗ:",            indexInstitute);
            addDataInView(description, c, "Факультет:",      indexDepartment);
            addDataInView(description, c, "Кафедра:",        indexChair);
            addDataInView(description, c, "Форма обучения:", indexEduFormName);
            addDataInView(description, c, "Квалификация:",   indexEduQualifName);
            addDataInView(description, c, "Год окончания:",  indexYear);

            table.addView(description);
        } while (c.moveToNext());
        c.close();

        return table;
    }

    private void addDataInView(LinearLayout parent, Cursor c, String name, int index){
        if(c.getString(index).length() > 0)
            parent.addView(createRow(name, c.getString(index)));
    }

    private ArrayList<String> getTablesNames(SQLiteDatabase db){
        ArrayList<String> tableNames = new ArrayList<>();

        Cursor c = db.query(SuaiAppConstants.EDUCATIONS_NAMES, new String[]{"name"},
                null, null, null, null, null);

        c.moveToFirst();

        int index = c.getColumnIndex("name");

        do tableNames.add(c.getString(index));
        while (c.moveToNext());

        c.close();

        return tableNames;
    }

    private LayoutParams setupLayout(int l, int t, int r, int b, float weight, boolean mode){
        LayoutParams viewParams;

        viewParams = mode
                ? new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                : new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        if(weight != -1)
            viewParams.weight = weight;

        viewParams.setMargins(dpToPx(l),dpToPx(t),dpToPx(r),dpToPx(b));

        return viewParams;
    }

    private int dpToPx(int dp){
        return (int) (dp * getContext().getResources().getDisplayMetrics().density);
    }
}
