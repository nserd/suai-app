package com.keaoc.nserd.suai_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LoadingActivity extends AppCompatActivity {

    static LoadingActivity loadingActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        loadingActivity = this;
    }

    @Override
    protected void onStop(){
        super.onStop();

        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();

        finish();
    }
}